#!/usr/bin/env python
# -*- coding: utf-8 -*-
from decimal import Decimal

__all__ = ['Product']


class Product(object):
    """
    An object that implements a descriptor for Product
    """
    def __init__(self, *args, **kwargs):
        self.name = kwargs.pop('Product Name')
        self.code = str(kwargs.pop('Product Code'))
        self._unit = kwargs.pop('Unit')
        self._weight = kwargs.pop('Weight')
        self.price = Decimal(str(kwargs.pop('Price')))
        self.vendor = kwargs.pop('Vendor')
        self.currency = kwargs.pop('Currency', None)

    @staticmethod
    def convert_to_grams(quantity, unit=None):
        """
        Helper function to make sure that we are always dealing
        with same unit of measurement along with given precision
        of decimal

        :param quantity: Numeric quantity that needs conversion
        :param unit: String that indicates the current UOM
        """
        # Assumption: Only expecting the unit to be either 'Kg' or 'Grams'
        # TODO: Allow converting from other units of same category (pound/ounce)

        if not unit:
            return quantity

        # Preserve precision (if any)
        quantity = Decimal(str(quantity))

        if unit.lower() == 'kg':
            return quantity * 1000
        elif unit.lower() == 'grams':
            return quantity

    @property
    def weight(self):
        return self.convert_to_grams(self._weight, self.unit)

    @property
    def unit(self):
        return 'grams'

    def pretty(self):
        # TODO: Reformat to include currency info as well
        pattern = '{vendor} - [{code}] {name} @ {price} / {_weight} {_unit}'
        return pattern.format(**self.__dict__)

    def __str__(self):
        return self.pretty()

    def __repr__(self):
        return self.pretty()

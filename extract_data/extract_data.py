#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

try:
    import unicodecsv as csv
except ImportError:
    import csv
from itertools import groupby
from collections import defaultdict

from .product import Product
from .utils import abs_file_path
from .exceptions import ResourceNotFound

__all__ = ['ExtractData']


class ExtractData(object):
    """
    Class to help interact with data from CSV File
    """
    def __init__(self):
        self.products = self.extract_data()
        self.products_by_code = self._get_products_grouped_by_code(self.products)

    def get_csv_file(self):
        """
        Fetch CSV file in following order of preference
        => From command line argument
        => Environment Variable called 'IMPORT_PATH'
        => Fallback to default file (hard-coded)
        """
        # Assumption: Expecting the input to be supplied as absoulte file path
        try:
            file = sys.argv[1]
        except IndexError:
            try:
                file = os.environ['IMPORT_PATH']
            except KeyError:
                file = abs_file_path('data/products.csv')

        return file

    def extract_data(self):
        """
        Extract data from CSV file and return a list of instances of 'Product'
        descriptor class
        """
        csv_file = self.get_csv_file()

        with open(csv_file, 'rbU') as csvfile:
            reader = csv.DictReader(csvfile)
            products = [Product(**row) for row in reader]

        return products

    @staticmethod
    def _get_products_grouped_by_code(products):
        """
        Helper method to group products by their code.
        This can be used later to find min/max price from a list.
        """
        key = lambda p: p.code
        products_by_code = defaultdict(list)
        for code, gproducts in groupby(sorted(products, key=key), key=key):
            products_by_code[code].extend(list(gproducts))

        return products_by_code

    def get_price(self, kind, code):
        """
        Method to return price (min/max) for given code of Product

        :param kind: min or max function
        :param code: Product code to fetch price for
        """
        if isinstance(code, int):
            code = str(code)

        try:
            product = kind(self.products_by_code[code], key=lambda p: p.price)
        except ValueError:
            # 'code' does not exist
            raise ResourceNotFound('Product [%s] is missing!' % code)

        return product

    def get_price_list(self, kind=min):
        """
        Return price list for given kind (min / max)
        """
        products_by_code = self.products_by_code
        products = []

        for code in products_by_code.keys():
            products.append(kind(products_by_code[code], key=lambda p: p.price))

        return products


if __name__ == '__main__':
    """
    Print three lists to stdout as requested
    """
    CODES = [3736, 4356, 3732, 3746, 3759, 3719, 3740, 4341]
    data = ExtractData()
    products_by_code = data.products_by_code

    mins = []
    maxs = []
    cheapest_for_given_codes = []

    for code in products_by_code.keys():
        min_price_product = min(products_by_code[code], key=lambda p: p.price)
        mins.append(min_price_product)
        if int(code) in CODES:
            cheapest_for_given_codes.append(min_price_product)

        maxs.append(max(products_by_code[code], key=lambda p: p.price))

    print "\nList of Cheapest Prices: \n"
    for product in mins:
        print product
    print "===================================================================="

    print "\nList of Expensive Prices: \n"
    for product in maxs:
        print product
    print "===================================================================="

    print "\nCheapest prices for products %s: \n" % CODES
    for product in cheapest_for_given_codes:
        print product
    print "===================================================================="

#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Ritesh Shrivastav'
__email__ = 'ritesh_shrv@live.com'
__version__ = '0.1.1'

from .extract_data import ExtractData  # noqa

# Extract Product Data
Python module that reads a CSV file and helps interact to it

## Requirements & Installation

Clone the repository and then install it.
Package requirements are handled using pip. To install them do

```
pip install -r requirements.txt
```

## Examples

```
from extract_data import ExtractData

data = ExtractData()

# To get pricelist based on minimum prices
data.get_price_list(min)

# To get pricelist based on maximum prices
data.get_price_list(max)

# To get minimum price of a product - 4371
data.get_price(min, 4371)

# To get maximum price of a product - 4371
data.get_price(max, 4371)
```

## Use different file
Make sure you are in module root and pass file's absolute path
```
eb_assignment/$ python -m extract_data.extract_data /Users/ritesh/products2.csv
```

Import file path can be set as environment variable as well
```
eb_assignment/$ export IMPORT_PATH=/Users/ritesh/products2.csv
eb_assignment/$ python -m extract_data.extract_data
```

## Tests
Make sure you are in module root and then run `py.test` from command line
```
eb_assignment/$ py.test
```

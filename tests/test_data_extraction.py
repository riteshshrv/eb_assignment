#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pytest

from extract_data import ExtractData
from extract_data.exceptions import ResourceNotFound


def test_data_extraction():
    data = ExtractData()

    # There's no argument passed nor env var set so, it should
    # read data from the existing file
    assert len(data.products)


def test_min_max_price_of_products():
    data = ExtractData()

    # Cheapest Price of product - 4356 is 29.5
    min_product = data.get_price(min, 4356)
    assert min_product.price == 29.5

    # Cheapest Price of product - 3736 is 80
    min_product = data.get_price(min, 3736)
    assert min_product.price == 80

    # Cheapest Price of product - 4365 is 29.5
    min_product = data.get_price(min, 4365)
    assert min_product.price == 31

    # Expensive Price of product - 3727 is 26
    max_product = data.get_price(max, 3727)
    assert max_product.price == 26

    # Expensive Price of product - 3790 is 39
    max_product = data.get_price(max, 3790)
    assert max_product.price == 39

    # Expensive Price of product - 3593 is 167
    max_product = data.get_price(max, 3593)
    assert max_product.price == 167


def test_pricelist():
    data = ExtractData()

    min_pricelist1 = data.get_price_list(min)
    assert min_pricelist1 is not None
    assert len(min_pricelist1) > 1

    # get_price_list return cheapest prices by default
    min_pricelist2 = data.get_price_list()
    assert min_pricelist2 is not None
    assert min_pricelist1 == min_pricelist2

    max_pricelist = data.get_price_list(min)
    assert max_pricelist is not None
    assert len(max_pricelist) > 1


def test_error_on_non_existent_product():
    data = ExtractData()

    with pytest.raises(ResourceNotFound):
        data.get_price(min, 'foo')

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()

requirements = [
    'unicodecsv',
]

test_requirements = [
    'pytest',
]

setup(
    name='eb-assignment',
    version='0.1.1',
    description="Python module that reads a CSV file and helps interact to it",
    long_description=readme,
    author="Ritesh Shrivastav",
    author_email='ritesh_shrv@live.com',
    url='',
    packages=[
        'extract_data',
    ],
    package_dir={'extract_data': 'extract_data'},
    include_package_data=True,
    install_requires=requirements,
    license="BSD license",
    keywords='eb-assignment',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
)
